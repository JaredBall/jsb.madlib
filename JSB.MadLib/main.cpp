// Lab Exercise 3 - Mad Lib
// Jared Ball

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

const int numInputs = 10;

string GenerateStory(string words[]);

enum WordType {
    Adjective1 = 0,
    Adjective2 = 1,
    Adjective3 = 2,
    VerbPast1 = 3,
    VerbPast2 = 4,
    Adverb = 5,
    Person = 6,
    Place = 7,
    Things = 8,
    Animal = 9
};

string WordTypeNames[numInputs] = {
    "an Adjective",
    "an Adjective",
    "an Adjective",
    "a Verb (past tense)",
    "a Verb (past tense)",
    "an Adverb",
    "a Person",
    "a Place",
    "some Things",
    "an Animal"
};

int main()
{
    string inputs[10];
    char saveToFile;
    for (int i = 0; i < numInputs; i++) 
    {
        cout << "Enter " << WordTypeNames[i] << ": ";
        getline(cin, inputs[i]);
    }
    string story = GenerateStory(inputs);
    cout << endl << story << endl << endl;
    cout << "Would you like to save output to file? (y/n): ";
    cin >> saveToFile;
    if (saveToFile == 'y' || saveToFile == 'Y') 
    {
        ofstream ofs("madlib.txt");
        ofs << story << endl;
        ofs.close();
        cout << "Mad lib has been saved to madlib.txt." << endl;
    }
    cout << endl << "Press any key to exit...";
    int _ = _getch();
    return 0;
}

string GenerateStory(string words[])
{
    return "On a " + words[WordType::Adjective1] + " day, " + words[WordType::Person] 
        + " was furiously coding a program about " + words[WordType::Things] + ".\n"
        + "After " + words[WordType::Person] + " finished, they watched some " 
        + words[WordType::Adjective2] + " YouTube videos.\n"
        + "In one video, an " + words[WordType::Animal] + " " + words[WordType::VerbPast1] 
        + " around " + words[WordType::Place] + ".\n"
        + words[WordType::Person] + " " + words[WordType::VerbPast2] + " " 
        + words[WordType::Adverb] + " while watching the videos.\n"
        + "Finally, " + words[WordType::Person] + " had a " + words[WordType::Adjective3] + " sleep.";
}

